package calculator;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {


    @org.junit.jupiter.api.Test
    void sum() {
        Calculator calc = new Calculator();
        assertEquals(2, calc.sum(1, 1), "1+1 must be 2");
    }
}